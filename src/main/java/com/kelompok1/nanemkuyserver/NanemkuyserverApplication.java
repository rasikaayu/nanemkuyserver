package com.kelompok1.nanemkuyserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class NanemkuyserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(NanemkuyserverApplication.class, args);
	}

}
